from django.shortcuts import render


def index(request):
    response = {}
    html = 'lab_8/lab_8.html'
    return render(request, html, response)
