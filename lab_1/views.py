from django.shortcuts import render
from datetime import datetime, date
from django.shortcuts import redirect
from django.urls import reverse


mhs_name = 'Norman'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 7, 6)


def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
