from django.shortcuts import render, redirect
from .models import Diary
from .forms import DiaryForm
from datetime import datetime
import pytz
import json
# Create your views here.
diary_dict = {}
def index(request):
    diary_dict = Diary.objects.all().values()
    diary_form = DiaryForm()
    return render(request, 'to_do_list.html', {'diary_dict': convert_queryset_into_json(diary_dict),
                                               'diary_form': diary_form})

def add_activity(request):
    if request.method == 'POST':
        diary_form = DiaryForm(request.POST)
        if diary_form.is_valid():
            date = diary_form.cleaned_data['date']
            activity = diary_form.cleaned_data['activity']
            Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=activity)

        return redirect('/lab-3/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val
