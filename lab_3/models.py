# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Diary(models.Model):
    date = models.DateTimeField()
    activity = models.TextField(max_length=60)
