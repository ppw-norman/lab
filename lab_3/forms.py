from django import forms


class DiaryForm(forms.Form):
    date = forms.DateTimeField(label="Tanggal",
                               input_formats=['%Y-%m-%dT%H:%M'])
    activity = forms.CharField(label="Kegiatan", max_length=60)
