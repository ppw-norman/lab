from django.test import TestCase
from django.urls import reverse
from django.urls import resolve
from .views import index, dashboard
import environ


root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')


def session_login(self, username, password):
    return self.client.post(reverse('lab-10-new:auth_login'), {'username': username, 'password': password})


class Lab10UnitTest(TestCase):

    def test_lab_10_login_url_before_login(self):
        response = self.client.get(reverse('lab-10-new:index'))
        self.assertEqual(response.status_code, 200)

    def test_lab_10_login_url_after_login(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        response = self.client.get(reverse('lab-10-new:index'))
        self.assertRedirects(response, reverse('lab-10-new:dashboard'))

    def test_lab_10_login_session_success(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        session = self.client.session
        self.assertEqual(session['user_login'], env("SSO_USERNAME"))

    def test_lab_10_logout(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        response = self.client.post(reverse('lab-10-new:auth_logout'))
        session = self.client.session
        self.assertEqual(session.get('user_login'), None)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-10-new:index'))

    def test_lab_10_login_session_fail(self):
        session_login(self, "asdf", "asdf")
        session = self.client.session
        self.assertEqual(session.get('user_login'), None)

    def test_lab_10_dashboard_before_login(self):
        response = self.client.get(reverse('lab-10-new:dashboard'))
        self.assertRedirects(response, reverse('lab-10-new:index'))

    def test_lab_10_dashboard_after_login(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        response = self.client.get(reverse('lab-10-new:dashboard'))
        self.assertEqual(response.status_code, 200)

    def test_lab_10_movie_list(self):
        response = self.client.get(reverse('lab-10-new:movie_list'))
        self.assertEqual(response.status_code, 200)
