# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class User(models.Model):
    code = models.CharField('Kode Identitas', max_length=20, primary_key=True,)
    name = models.CharField('Nama', max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class WatchLaterMovie(models.Model):
    user = models.ForeignKey(User, related_name='watch_later_movies')
    code = models.CharField("Kode Movie", max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
