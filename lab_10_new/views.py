# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse

from .models import User
from .omdb_api import get_detail_movie, search_movie, get_movies_detail
from .custom_auth import get_user_from_session

response = {}


def index(request):
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('lab-10-new:dashboard'))

    response['author'] = request.session.get('user_login')
    html = 'lab_10_new/login.html'
    return render(request, html, response)


def dashboard(request):
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('lab-10-new:index'))

    response['author'] = request.session.get('user_login')
    response['identity_number'] = request.session.get('identity_number')
    response['role'] = request.session.get('role')

    identity_number = request.session.get('identity_number')

    user, created = User.objects.get_or_create(
        code=identity_number,
        defaults={
            'name': request.session.get('user_login'),
        }
    )

    movie_ids = request.session.get('movie_ids', [])
    for movie_id in movie_ids:
        user.watch_later_movies.get_or_create(code=movie_id)

    html = 'lab_10_new/dashboard.html'
    return render(request, html, response)


def movie_list(request):
    judul = request.GET.get("judul", "-")
    tahun = request.GET.get("tahun", "-")
    urlDataTables = "/lab-10/api/movie/" + judul + "/" + tahun
    jsonUrlDataTables = json.dumps(urlDataTables)
    response['jsonUrlDT'] = jsonUrlDataTables
    response['judul'] = judul
    response['tahun'] = tahun
    response['author'] = request.session.get('user_login')

    html = 'lab_10_new/movie/list.html'
    return render(request, html, response)


def movie_detail(request, id):
    response['id'] = id
    user = get_user_from_session(request)

    if user is not None:
        is_added = user.watch_later_movies.filter(code=id).count() > 0
    else:
        is_added = id in request.session.get('movies', [])

    response['is_added'] = is_added
    response['movie'] = get_detail_movie(id)
    html = 'lab_10_new/movie/detail.html'
    return render(request, html, response)


def add_watch_later(request, id):
    msg = "Berhasil tambah movie ke Watch Later"

    user = get_user_from_session(request)
    if user is not None:
        user.watch_later_movies.get_or_create(code=id)
    else:
        movies = set(request.session.get('movies', []))
        movies.add(id)
        request.session['movies'] = list(movies)

    messages.success(request, msg)
    return HttpResponseRedirect(reverse('lab-10-new:movie_detail', args=(id,)))


def list_watch_later(request):
    user = get_user_from_session(request)
    response['author'] = request.session.get('user_login')

    if user is not None:
        movie_ids = [movie.code for movie in user.watch_later_movies.all()]
    else:
        movie_ids = request.session.get('movies', [])

    watch_later_movies = get_movies_detail(movie_ids)

    response['watch_later_movies'] = watch_later_movies
    html = 'lab_10_new/movie/watch_later.html'
    return render(request, html, response)


def api_search_movie(request, judul, tahun):
    search_results = search_movie(judul, tahun)
    items = search_results

    dataJson = json.dumps({"dataku": items})
    mimetype = 'application/json'
    return HttpResponse(dataJson, mimetype)
