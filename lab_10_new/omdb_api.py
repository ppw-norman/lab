import requests
from math import ceil
API_KEY = "9caf83af"


def search_movie(judul, tahun):
    url = "http://www.omdbapi.com/?s=" + judul + "&y=" + tahun + "&apikey=" + API_KEY
    response = requests.get(url).json()

    is_data_exist = response.get("Response") == "True"

    results = []
    if is_data_exist:
        count_results = int(response['totalResults'])

        for page in range(min(ceil(count_results / 10), 3)):
            paged_url = url + "&page=" + str(page + 1)
            response = requests.get(paged_url).json()
            results += response["Search"]

    return results


def get_detail_movie(id):
    url = "http://www.omdbapi.com/?i=" + id + "&apikey=" + API_KEY
    response = requests.get(url).json()

    return response


def get_movies_detail(movie_ids):
    return [get_detail_movie(movie_id) for movie_id in movie_ids]
